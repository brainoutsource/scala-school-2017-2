package wtf.scala.lectures

package object e04 {

  class Animal { def sound: String = "rrr" }

  class Dog extends Animal { override val sound = "wuf" }

  class Cat extends Animal { override val sound = "meow" }

}
