package wtf.scala.e06

object SeqModifications {

  /**
    * Duplicate every element of seq n times
    * @param n
    * @param s
    * @return
    */
  def duplicateN(n: Int, s: Seq[Int]): Seq[Int] = ???

  /**
    * Remove every n-th element of seq
    * @param n
    * @param s
    * @return
    */
  def removeN(n: Int, s: Seq[Int]): Seq[Int] = ???

  /**
    * Rotate seq by n to the left
    * @param n
    * @param s
    * @return
    */
  def rotate(n: Int, s: Seq[Int]): Seq[Int] = ???

}
