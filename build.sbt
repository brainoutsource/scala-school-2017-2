name := "scala-school-2017-2"

version := "1.0"

scalaVersion := "2.12.1"

// scalaVersion := "2.11.8" // for spark and salat

libraryDependencies ++=  Seq(
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
  "com.storm-enroute" %% "scalameter" % "0.8.2" % "test" excludeAll ExclusionRule(organization = "org.mongodb"),
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "com.typesafe.akka" % "akka-stream_2.12" % "2.4.17",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.17" % "test",
  "com.typesafe.akka" %% "akka-http" % "10.0.5",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.5" % "test",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.5",
  "io.spray" %% "spray-json" % "1.3.3",
  "com.h2database" % "h2" % "1.4.194",
  "com.typesafe.slick" %% "slick" % "3.2.0",
  "org.tpolecat" %% "doobie-core" % "0.4.1",
  "org.scalikejdbc" %% "scalikejdbc" % "2.5.1",
  "org.scalikejdbc" %% "scalikejdbc-config" % "2.5.1",
  "org.scalikejdbc" %% "scalikejdbc-test" % "2.5.1" % "test",
  "ch.qos.logback" % "logback-classic" % "1.2.1",
  "com.typesafe.play" %% "anorm" % "2.5.3",
  "org.mongodb" %% "casbah" % "3.1.1"
//  "com.novus" %% "salat" % "1.9.9" // scala 2.11 required
)

testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework")

parallelExecution in Test := false
